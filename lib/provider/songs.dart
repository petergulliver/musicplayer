import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:musicplayer/model/song.dart';

//make this class a notifier so we can track error
class SongsProvider with ChangeNotifier {
  final http.Client client;
  String error;

  SongsProvider({this.client});

  bool get hasError => (error != null);

  String _searchTerm = '';

  set searchTerm(String s) {
    if (_searchTerm == s) return; //don't update it if not necessary
    _searchTerm = s ?? '';
    notifyListeners();
  }

  String get searchTerm => _searchTerm;

  Future<List<Song>> songList(String searchTerm, [int limit = 50]) async {
    print('SongsProvider.songList :: $searchTerm');

    if (searchTerm.isEmpty)
      return []; //don't call the API if there is no search term

    List<Song> result = [];

    error = null;

    var url = Uri.https('itunes.apple.com', '/search', {
      'media': 'music',
      'entity': 'song',
      'attribute': 'artistTerm',
      'limit': '$limit',
      'term': searchTerm
    });

    try {
      print('SongsProvider.songList.getSongs :: $url');
      //need to be able to pass a client in so we can test
      http.Client httpClient = client ?? http.Client();
      var response = await httpClient.post(url);

      if (response.statusCode == 200) {
        // print('SongsProvider.songList.getSongs 1 :: ${response.body}');
        var jsonResponse = json.decode(response.body);
        // print('SongsProvider.songList.getSongs 2 :: $jsonResponse');
        var resultCount = jsonResponse['resultCount'];
        result = List.from(jsonResponse['results'])
            .map((s) => Song.fromMap(s))
            .toList();
        print(
            'SongsProvider.songList.response :: $resultCount : ${result.length}');
      } else {
        print('Request failed with status: ${response.statusCode}.');
        error = 'request failed (${response.statusCode})';
      }
    } catch (e) {
      print(e);
      error = e.toString(); //could make this more elegant
    }
    return result;
  }
}
