import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:musicplayer/model/song.dart';
import 'package:musicplayer/provider/songs.dart';
import 'package:musicplayer/provider/state.dart';
import 'package:musicplayer/widget/song_list.dart';

import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  HomePage();

  @override
  Widget build(BuildContext context) {
    final SongsProvider _songs = Provider.of<SongsProvider>(context);

    return MultiProvider(
        providers: [
          FutureProvider<List<Song>>.value(
            initialData: [],
            value: _songs.songList(_songs.searchTerm),
            catchError: (context, error) => [],
          ),
        ],
        child: Scaffold(
            // backgroundColor: Colors.grey[200],
            appBar: new AppBar(
              elevation: 1.0,
              leading: Container(
                padding: EdgeInsets.all(8.0),
                child: Image.asset(
                  "assets/images/logo.128.t.png",
                ),
              ),
              title: Text('musicplayer'),
            ),
            body: Container(
                child: Column(
              children: [
                // the search field
                Container(
                    padding: EdgeInsets.all(20),
                    alignment: Alignment.center,
                    //styling of cupertino is better here rather than Material
                    child: CupertinoTextField(
                      key: Key('SearchField'),
                      style: TextStyle(fontSize: 22),
                      autofocus: true,
                      onSubmitted: (text) {
                        _songs.searchTerm = text;
                      },
                    )),
                //the song list
                Expanded(
                  child: Consumer<List<Song>>(
                      builder: (context, _songList, child) {
                    return SongList(songList: _songList);
                  }),
                ),
              ],
            ))));
  }
}
