import 'package:flutter/material.dart';
import 'package:musicplayer/model/song.dart';
import 'package:musicplayer/widget/fade_network_image.dart';

class SongTile extends StatelessWidget {
  const SongTile({@required this.song, @required this.onTap});

  final Song song;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        margin: EdgeInsets.all(5),
        elevation: 0,
        color: Colors.white.withOpacity(0.2),
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          leading: FadeNetworkImage(
              elevation: 2, height: 120, url: song?.imageUrl ?? ''),
          title: Text(song?.title ?? 'title',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          subtitle:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            SizedBox(height: 5),
            Text(song?.artist ?? 'artist',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal)),
            SizedBox(height: 5),
            Text(song?.album ?? 'album',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w200))
          ]),
          trailing: song?.selected ?? false
              ? Icon(
                  song?.playing ?? false
                      ? Icons.play_arrow_rounded
                      : Icons.pause_rounded,
                  size: 40,
                  color: Colors.blueGrey.shade300,
                )
              : null,
          isThreeLine: false,
          onTap: () {
            onTap(song);
          },
        ));
  }
}
