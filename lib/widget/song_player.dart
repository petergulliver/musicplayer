import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:musicplayer/model/song.dart';

class SongPlayer extends StatefulWidget {
  const SongPlayer({@required this.song, this.onPlayingChange});

  final Song song;
  final Function onPlayingChange;

  @override
  _SongPlayerState createState() => _SongPlayerState();
}

class _SongPlayerState extends State<SongPlayer> {
  final AudioPlayer _player = AudioPlayer();

  @override
  void initState() {
    super.initState();
    _initPlayer(widget.song);
  }

  @override
  void didUpdateWidget(SongPlayer ow) {
    super.didUpdateWidget(ow);
    if (widget.song?.id != ow.song?.id) {
      //reload the player
      _initPlayer(widget.song);
    }
  }

  void _initPlayer(Song song) {
    // print('SongPlayer._initPlayer :: ${song?.title}');
    _player.setUrl(song?.previewUrl ?? '');
    _player.setLoopMode(LoopMode.one);
    _player.play();
    // print('SongPlayer._initPlayer :: ${_player.playing}');
  }

  @override
  void dispose() {
    super.dispose();
    _player.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.8),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
            )),
        height: 150,
        // color: Colors.white.withOpacity(0.8),
        padding: EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton(
              style: TextButton.styleFrom(
                  shape: CircleBorder(), padding: EdgeInsets.all(10)),
              child: Icon(
                _player.playing
                    ? Icons.pause_circle_filled_rounded
                    : Icons.play_circle_filled_rounded,
                size: 80,
                color: Colors.blueGrey.shade500,
              ),
              onPressed: () {
                // print('SongPlayer.onPressed :: ${_player.playing}');
                if (_player.playing)
                  _player.pause();
                else
                  _player.play();
                setState(() {
                  //update the state
                });
                if (widget.onPlayingChange != null)
                  widget.onPlayingChange(_player.playing);
              },
            ),
            SizedBox(height: 5),
            Text('${widget.song?.title ?? 'no song selected'}',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style:
                    TextStyle(fontSize: 18, color: Colors.blueGrey.shade800)),
          ],
        ));
  }
}
