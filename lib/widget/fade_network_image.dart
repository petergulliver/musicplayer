import 'package:flutter/material.dart';

class FadeNetworkImage extends StatelessWidget {
  FadeNetworkImage(
      {@required this.url,
      this.width,
      this.height,
      this.elevation,
      this.borderColor,
      this.borderRadius});

  final String url;
  final double width;
  final double height;
  final double elevation;
  final Color borderColor;
  final double borderRadius;

  @override
  Widget build(BuildContext context) {
    String _defaultimagepath = 'assets/images/defaultthumbnail.png';
    ImageProvider _imageProvider = url?.isEmpty ?? true
        ? AssetImage(_defaultimagepath)
        : NetworkImage(url);
    return Card(
        elevation: elevation ?? 0,
        borderOnForeground: false,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 5)),
        color: Colors.transparent,
        child: FadeInImage(
            width: width,
            height: height,
            fit: BoxFit.fill,
            placeholder: AssetImage(_defaultimagepath),
            image: _imageProvider)); //);
  }
}
