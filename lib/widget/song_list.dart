import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:musicplayer/model/song.dart';
import 'package:musicplayer/widget/song_player.dart';
import 'package:musicplayer/widget/song_tile.dart';

class SongList extends StatefulWidget {
  final List<Song> songList;
  SongList({this.songList});

  @override
  _SongListState createState() => _SongListState();
}

class _SongListState extends State<SongList> {
  Song _playingSong;
  Song _selectedSong;

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.bottomCenter, children: <Widget>[
      Container(
          child: widget.songList?.isNotEmpty ?? false
              ? ListView(
                  children: widget.songList.map((Song s) {
                  return SongTile(
                    song: s,
                    onTap: (Song s) {
                      print('selected song :: ${s?.title}');
                      if (s != null) //safety, shouldn't happen
                        setState(() {
                          _selectedSong?.selected = false;
                          s?.selected = true;
                          s?.playing = true;
                          _playingSong = s?.clone();
                          _selectedSong = s;
                        });
                      // _songs.selectedSong = s;
                    },
                  );
                }).toList())
              : SizedBox()),
      if (_playingSong != null)
        SongPlayer(
          song: _playingSong,
          onPlayingChange: (playing) {
            setState(() {
              _selectedSong?.playing = playing;
            });
          },
        )
    ]);
  }
}
