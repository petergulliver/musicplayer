class Song {
  // make class property names readable
  final int id;
  final String title;
  final String artist;
  final String album;
  final String previewUrl;
  final String imageUrl;

  bool playing = false;
  bool selected = false;

  Song(
      {this.id,
      this.title,
      this.artist,
      this.album,
      this.previewUrl,
      this.imageUrl});

  Song.fromMap(Map json)
      : id = json['trackId'],
        title = json['trackName'],
        artist = json['artistName'],
        album = json['collectionName'],
        previewUrl = json['previewUrl'],
        imageUrl = json['artworkUrl100'];

  Song.fromJSON(Map json)
      : id = json['id'],
        title = json['title'],
        artist = json['artist'],
        album = json['album'],
        previewUrl = json['previewUrl'],
        imageUrl = json['imageUrl'];

  Map toJSON() {
    return {
      'id': id,
      'title': title,
      'artist': artist,
      'album': album,
      'previewUrl': previewUrl,
      'imageUrl': imageUrl
    };
  }

  String toString() {
    return toJSON().toString();
  }

  //create another Song object to preserve it in case the original object is destroyed
  Song clone() {
    return Song.fromJSON(toJSON());
  }
}
