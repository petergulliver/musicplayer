import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:musicplayer/page/home.dart';
import 'package:musicplayer/provider/songs.dart';
import 'package:musicplayer/provider/state.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('assets/fonts/Lato/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  //fix portait mode
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MusicPlayerApp());
}

class MusicPlayerApp extends StatelessWidget {
  final mainColor = MaterialColor(0xFF4B154D, const <int, Color>{
    50: const Color(0xFFEBB5ED),
    100: const Color(0xFFCB95CD),
    200: const Color(0xFFAB75AD),
    300: const Color(0xFF8B558D),
    400: const Color(0xFF6B356D),
    500: const Color(0xFF4B154D),
    600: const Color(0xFF2B052D),
    700: const Color(0xFF0B030D),
    800: const Color(0xFF060208),
    900: const Color(0xFF020004),
  });

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<StateProvider>(create: (_) => StateProvider()),
          ChangeNotifierProvider<SongsProvider>(create: (_) => SongsProvider()),
        ],
        child: MaterialApp(
            title: 'musicplayer',
            debugShowCheckedModeBanner: false,
            theme: new ThemeData(
              primarySwatch: mainColor,
              accentColor: Colors.cyan[600],
              buttonTheme: ButtonThemeData(
                buttonColor: mainColor, //  <-- dark color
                textTheme: ButtonTextTheme
                    .primary, //  <-- this auto selects the right color
              ),
              scaffoldBackgroundColor: Colors.grey[800],
              dialogTheme: DialogTheme(
                  backgroundColor: Colors.white,
                  titleTextStyle:
                      TextStyle(color: Colors.grey[800], fontSize: 18),
                  contentTextStyle:
                      TextStyle(color: Colors.blueGrey, fontSize: 14)),
              textTheme: GoogleFonts.latoTextTheme(
                Theme.of(context)
                    .textTheme
                    .apply(bodyColor: Colors.white, displayColor: Colors.white),
              ),
            ),
            home: HomePage()));
  }
}
