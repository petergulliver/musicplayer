import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
// import 'package:test/test.dart';
import 'package:http/http.dart' as http;

// import 'package:flutter_test/flutter_test.dart';
import 'package:musicplayer/page/home.dart';
import 'package:musicplayer/provider/songs.dart';
import 'package:musicplayer/provider/state.dart';
import 'package:musicplayer/widget/song_list.dart';
import 'package:musicplayer/widget/song_tile.dart';
import 'package:provider/provider.dart';

import '../page/home_test.mocks.dart';

Widget createHomeScreen(http.Client client) => MultiProvider(
      providers: [
        ChangeNotifierProvider<StateProvider>(create: (_) => StateProvider()),
        ChangeNotifierProvider<SongsProvider>(
            create: (_) => SongsProvider(client: client)),
      ],
      child: Builder(
        builder: (_) => MaterialApp(
          home: HomePage(),
        ),
      ),
    );

@GenerateMocks([http.Client])
void main() {
  group('HomePage Widget Tests', () {
    final client = MockClient();

    var uri = Uri.https('itunes.apple.com', '/search', {
      'media': 'music',
      'entity': 'song',
      'attribute': 'artistTerm',
      'limit': '50',
      'term': 'The Beatles'
    });

    var uriBlank = Uri.https('itunes.apple.com', '/search', {
      'media': 'music',
      'entity': 'song',
      'attribute': 'artistTerm',
      'limit': '50',
      'term': ''
    });

    // print('home_test :: $uriBlank');

    //generate some JSON
    String response = '''{      
      "resultCount": 3,
      "results": [
        {
          "trackId": 111,
          "trackName": "Track 1",
          "artistName": "The Beatles",
          "collectionName": "Album",
          "previewUrl": "",
          "artworkUrl100": ""
        },
        {
          "trackId": 222,
          "trackName": "Track 2",
          "artistName": "The Beatles",
          "collectionName": "Album",
          "previewUrl": "",
          "artworkUrl100": ""
        },
        {
          "trackId": 333,
          "trackName": "Track 3",
          "artistName": "The Beatles",
          "collectionName": "Album",
          "previewUrl": "",
          "artworkUrl100": ""
        }
      ]
    }''';

    String responseBlank = '''{      
      "resultCount": 0,
      "results": []
    }''';

    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(uri))
        .thenAnswer((_) async => http.Response(response, 200));
    when(client.post(uriBlank))
        .thenAnswer((_) async => http.Response(responseBlank, 200));

    testWidgets('Testing if SongList exists', (tester) async {
      await tester.pumpWidget(createHomeScreen(client));
      expect(find.byType(SongList), findsOneWidget);
    });

    testWidgets('Testing if Search field exists', (tester) async {
      await tester.pumpWidget(createHomeScreen(client));
      expect(find.byType(CupertinoTextField), findsOneWidget);
      expect(find.byKey(Key('SearchField')), findsOneWidget);
    });

    testWidgets('Testing if Search returns the list', (tester) async {
      await tester.pumpWidget(createHomeScreen(client));
      var searchField = find.byKey(Key('SearchField'));
      await tester.tap(searchField); // acquire focus
      await tester.enterText(searchField, 'The Beatles');
      expect(find.text('The Beatles'), findsOneWidget);
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pumpAndSettle();
      //the ListView is only rendered if songs are present in list
      expect(
          find.descendant(
              of: find.byType(SongList), matching: find.byType(ListView)),
          findsOneWidget);
      //there should be some SongTiles in the SongList
      expect(
          find.descendant(
              of: find.byType(SongList), matching: find.byType(SongTile)),
          findsWidgets);
    });
  });
}
