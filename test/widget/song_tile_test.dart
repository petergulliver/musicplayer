import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:musicplayer/model/song.dart';
import 'package:musicplayer/widget/song_tile.dart';

Widget createSongTile(Song song) => Directionality(
      child: MediaQuery(
          data: MediaQueryData(), child: SongTile(song: song, onTap: () {})),
      textDirection: TextDirection.ltr,
    );

void main() {
  Song song = Song.fromMap({
    "trackId": 111,
    "trackName": "Track 1",
    "artistName": "The Beatles",
    "collectionName": "Album",
    "previewUrl": "",
    "artworkUrl100": ""
  });

  testWidgets('Testing if SongTile builds when Song is null', (tester) async {
    await tester.pumpWidget(createSongTile(null));
    expect(find.byType(ListTile), findsOneWidget);
  });

  testWidgets('Testing if SongTile builds when Song is empty', (tester) async {
    await tester.pumpWidget(createSongTile(Song()));
    expect(find.byType(ListTile), findsOneWidget);
  });

  testWidgets('Testing if SongTile builds when Song has properties',
      (tester) async {
    await tester.pumpWidget(createSongTile(song));
    expect(find.byType(ListTile), findsOneWidget);
  });

  testWidgets('Testing if SongTile shows the song artist, title and album',
      (tester) async {
    await tester.pumpWidget(createSongTile(song));
    expect(find.text('The Beatles'), findsOneWidget);
    expect(find.text('Track 1'), findsOneWidget);
    expect(find.text('Album'), findsOneWidget);
  });
}
