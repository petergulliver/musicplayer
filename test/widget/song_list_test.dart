import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:musicplayer/model/song.dart';
import 'package:musicplayer/widget/song_list.dart';
import 'package:musicplayer/widget/song_player.dart';
import 'package:musicplayer/widget/song_tile.dart';

Widget createSongList(List<Song> list) => Directionality(
      child: MediaQuery(
        data: MediaQueryData(),
        child: SongList(songList: list),
      ),
      textDirection: TextDirection.ltr,
    );

void main() {
  List<Song> list = [
    Song.fromMap({
      "trackId": 111,
      "trackName": "Track 1",
      "artistName": "The Beatles",
      "collectionName": "Album",
      "previewUrl": "",
      "artworkUrl100": ""
    }),
    Song.fromMap({
      "trackId": 222,
      "trackName": "Track 2",
      "artistName": "The Beatles",
      "collectionName": "Album",
      "previewUrl": "",
      "artworkUrl100": ""
    }),
    Song.fromMap({
      "trackId": 333,
      "trackName": "Track 3",
      "artistName": "The Beatles",
      "collectionName": "Album",
      "previewUrl": "",
      "artworkUrl100": ""
    })
  ];

  testWidgets('Testing if SongList builds without ListView when list is empty',
      (tester) async {
    await tester.pumpWidget(createSongList([]));
    expect(find.byType(ListView), findsNothing);
  });

  testWidgets('Testing if SongList builds with ListView when list has items',
      (tester) async {
    await tester.pumpWidget(createSongList(list));
    expect(find.byType(ListView), findsOneWidget);
    //make sure the SongTiles were created
    expect(find.byType(SongTile), findsNWidgets(3));
    //text should appear 3 times
    expect(find.text('The Beatles'), findsNWidgets(3));
  });

  testWidgets('Testing if SongPlayer pops up when SongTile is tapped',
      (tester) async {
    await tester.pumpWidget(createSongList(list));
    var songTile = find.byType(SongTile).first;
    print('testing tap songtile : ${songTile.evaluate().single.widget}');
    expect(songTile, findsOneWidget);
    await tester.tap(songTile);
    // following fails due to issues with AudioPlayer...
    // await tester.pump();
    // expect(find.byType(SongPlayer), findsOneWidget);
  });
}
