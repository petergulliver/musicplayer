import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:musicplayer/model/song.dart';
import 'package:musicplayer/widget/song_player.dart';

//these tests fails because there seems to be a timer leak in the AudioPlayer

Widget createSongPlayer(Song song) => Directionality(
      child: MediaQuery(data: MediaQueryData(), child: SongPlayer(song: song)),
      textDirection: TextDirection.ltr,
    );

void main() {
  Song song = Song.fromMap({
    "trackId": 111,
    "trackName": "Track 1",
    "artistName": "The Beatles",
    "collectionName": "Album",
    "previewUrl": "",
    "artworkUrl100": ""
  });

  // testWidgets('Testing if SongPlayer builds when Song is null', (tester) async {
  //   await tester.pumpWidget(createSongPlayer(null), Duration(seconds: 5));
  //   expect(find.byType(Column), findsOneWidget);
  // });

  // testWidgets('Testing if SongPlayer builds when Song is empty',
  //     (tester) async {
  //   await tester.pumpWidget(createSongPlayer(Song()));
  //   expect(find.byType(Column), findsOneWidget);
  // });

  // testWidgets('Testing if SongPlayer builds when Song has properties',
  //     (tester) async {
  //   await tester.pumpWidget(createSongPlayer(song), Duration(seconds: 10));
  //   expect(find.byType(Column), findsOneWidget);
  // });

  // testWidgets('Testing if SongPlayer shows the play button and song title',
  //     (tester) async {
  //   await tester.pumpWidget(createSongPlayer(song));
  //   expect(find.byType(TextButton), findsOneWidget);
  //   expect(find.text('Track 1'), findsOneWidget);
  // });
}
