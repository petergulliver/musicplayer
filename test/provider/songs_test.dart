import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';
import 'package:http/http.dart' as http;

import 'package:musicplayer/model/song.dart';
import 'package:musicplayer/provider/songs.dart';

import 'songs_test.mocks.dart';

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
@GenerateMocks([http.Client])
void main() {
  test('test the songs provider', () async {
    final client = MockClient();

    var uri = Uri.https('itunes.apple.com', '/search', {
      'media': 'music',
      'entity': 'song',
      'attribute': 'artistTerm',
      'limit': '3',
      'term': 'The Beatles'
    });

    //generate some JSON
    String response = '''{      
      "resultCount": 3,
      "results": [
        {
          "trackId": 111,
          "trackName": "Track 1",
          "artistName": "The Beatles",
          "collectionName": "Album",
          "previewUrl": "",
          "artworkUrl100": ""
        },
        {
          "trackId": 222,
          "trackName": "Track 2",
          "artistName": "The Beatles",
          "collectionName": "Album",
          "previewUrl": "",
          "artworkUrl100": ""
        },
        {
          "trackId": 333,
          "trackName": "Track 3",
          "artistName": "The Beatles",
          "collectionName": "Album",
          "previewUrl": "",
          "artworkUrl100": ""
        }
      ]
    }''';

    // Use Mockito to return a successful response when it calls the
    // provided http.Client.
    when(client.post(uri))
        .thenAnswer((_) async => http.Response(response, 200));

    SongsProvider provider = SongsProvider(client: client);
    List<Song> songList = await provider.songList('The Beatles', 3);
    print('songs.test :: songlist length : ${songList.length}');
    expect(provider.hasError, equals(false));
    expect(songList.length, greaterThan(0));
    songList.forEach((s) {
      print('songs.test : id : artist :: ${s.id} : ${s.artist}');
      expect(s.artist.compareTo('The Beatles'), equals(0));
    });
  });
}
