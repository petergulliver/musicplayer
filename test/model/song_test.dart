import 'package:flutter/foundation.dart';
import 'package:test/test.dart';

import 'package:musicplayer/model/song.dart';

void main() {
  test('test creating song model', () {
    Song song = Song(
        id: 1234,
        title: 'song title',
        artist: 'song artist',
        album: 'song album',
        previewUrl: 'song previewUrl',
        imageUrl: 'song imageUrl');

    expect(song.id, equals(1234));
    expect(song.title, equals('song title'));
    expect(song.artist, equals('song artist'));
    expect(song.album, equals('song album'));
    expect(song.previewUrl, equals('song previewUrl'));
    expect(song.imageUrl, equals('song imageUrl'));
    print('song.test :: song created');
  });

  test('test cloning song model', () {
    Song song = Song(
        id: 1234,
        title: 'song title',
        artist: 'song artist',
        album: 'song album',
        previewUrl: 'song previewUrl',
        imageUrl: 'song imageUrl');

    Song clonedsong = song.clone();
    //make sure the clone has the same property values
    expect(mapEquals(song.toJSON(), clonedsong.toJSON()), equals(true));
    print('song.test :: song cloned ok');
  });
}
