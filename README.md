# musicplayer

A basic music player app written in Flutter for Candidate Test for BCG.

## Objective

To develop a simple music player app that uses the iTunes affiliate API to search songs by artist. The search results will be displayed on the UI as a list of tiles, each tile with information about the song including title, artist, album title and album art. When a song is selected from the list, the song will start to play and the player will persist during further searches until another song is selected.

The app is to run on an Android emulator.

## Requirements

* Each song’s title, artist, album and album art should be displayed.
* When a song is tapped, a media player should show up at the bottom of the screen and start to play the preview for that song. 
* The media player may be something as simple as a toggling play or pause button.
* The media player should pop-up at the bottom of the screen and on top of the list
* The media player should only show once a song is clicked and should stay on the screen from that point onwards and should be reused for any subsequent song played.
* When a song is being played, there must be an indicator in the list item that the song is being played.
* Playback can be stopped if a new search is performed, however the preference is for the song to keep playing.
* If playback is stopped when a new search is performed, the media player must be hidden until a new song is selected.
* Necessary tests for a mobile app should be included.

## Supported Devices

This app was tested on the following devices

* Nexus 5X Android Emulator running API 30.
* Pixel 2 XL running API 27
* Samsung SM A908B mobile phone

## Building the app

### Requirements

This app was developed and tested with Flutter 2.0.2 (channel stable). To download the Flutter SDK go to the following link : https://flutter.dev/docs/get-started/install.

To run the app on Android devices the Android SDK is required. It can be downloaded here https://developer.android.com/studio

### Instructions

* Download and install Flutter.
* Download and install Android Studio.
* Clone the repository from https://bitbucket.org/petergulliver/musicplayer.git.
* Open the Android project in Android Studio (the `android` subdirectory of the project) and set the required SDK and NDK locations from File > Project Structure.
* In the root directory of the project run the following:
```
flutter run -d devicename
```
where ```devicename``` is the name of the required device. 

To see a list of available devices run the following:
```
flutter devices
```

### Testing

To run the unit tests for the app run the following:
```
flutter test
```

### App Launcher Icons

To change the App Launcher Icons edit or replace the file ```assets/images/logo.1024.png``` (1024x1024px) and run the following:
```
flutter pub run flutter_launcher_icons:main
```
